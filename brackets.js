/*

Brackets - https://codility.com/c/run/training4FTTXB-7HP

A string S consisting of N characters is considered to be properly nested if any of the following conditions is true:

S is empty;
S has the form "(U)" or "[U]" or "{U}" where U is a properly nested string;
S has the form "VW" where V and W are properly nested strings.
For example, the string "{[()()]}" is properly nested but "([)()]" is not.

Write a function:

function solution(S);

that, given a string S consisting of N characters, returns 1 if S is properly nested and 0 otherwise.

For example, given S = "{[()()]}", the function should return 1 and given S = "([)()]", the function should return 0, as explained above.

Assume that:

N is an integer within the range [0..200,000];
string S consists only of the following characters: "(", "{", "[", "]", "}" and/or ")".
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N) (not counting the storage required for input arguments).
Copyright 2009–2016 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.

*/


// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(S) {
  // write your code in JavaScript (Node.js 4.0.0)

  var original_stack = S.split("");
  var test_group;
  var stack = [];

  if (S === '') {
    return 1;
  } else {
    for (var i in original_stack) {
      if (original_stack[i] === '(' || original_stack[i] === '{' || original_stack[i] === '[') {
        stack.push(original_stack[i]);
      } else {
        test_group = stack.pop() + original_stack[i];

        if (test_group !== '()' && test_group !== '{}' && test_group !== '[]') {
          return 0;
        }

      }
    }
  }

  if (stack.length === 0) {
    return 1;
  }

  return 0;

}